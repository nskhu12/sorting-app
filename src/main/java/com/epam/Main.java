package com.epam;

public class Main {
    public static void main(String[] args) {
        SortingApp sorter = new SortingApp(args);
        sorter.sort();
    }
}