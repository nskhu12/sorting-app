package com.epam;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class SortingApp {
    private static final Logger logger = Logger.getLogger(SortingApp.class);
    // TODO: 6/5/2023 warning here
    private String[] args;
    private String result = "";


    public SortingApp(String[] args) {
        this.args = args;
        // Configure log4j
        PropertyConfigurator.configure("src/main/resources/log4j.properties");
    }

    public void sort(){
        if (args.length > 10) {
            result = "Error: Too many arguments. Maximum of 10 arguments allowed.";
            System.out.println(result);
            logger.debug(result);
            return;
        }
        if (args.length == 0) {
            result = "Error: zero arguments. minimum of 1 arguments allowed.";
            logger.debug(result);
            System.out.println(result);
            return;
        }

        int[] numbers = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            try {
                numbers[i] = Integer.parseInt(args[i]);
            } catch (NumberFormatException e) {
                result = "Error: Invalid integer argument provided.";
                logger.debug(result);
                System.out.println(result);
                return;
            }
        }

        BubbleSort.bubbleSort(numbers);

        for (int number : numbers) {
            // TODO: 6/5/2023 warning here
            result += (number + " ");
        }
        logger.debug(result);
        System.out.println(result);
    }

    public String getResult() {
        return result;
    }
}
