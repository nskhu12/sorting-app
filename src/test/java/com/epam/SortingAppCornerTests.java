package com.epam;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SortingAppCornerTests {
    @Test
    public void testEmptyArray() {
        String[] arr = {};
        SortingApp sorter = new SortingApp(arr);
        sorter.sort();
        String expected = "Error: zero arguments. minimum of 1 arguments allowed.";
        assertEquals(expected, sorter.getResult());
    }

    @Test
    public void testSingleElementArray() {
        String[] arr = {"5"};
        SortingApp sorter = new SortingApp(arr);
        sorter.sort();
        String expected = "5 ";
        assertEquals(expected, sorter.getResult());
    }

    @Test
    public void testTenElementsArray() {
        String[] arr =  {"9", "7", "2", "8", "1", "6", "4", "5", "3", "0"};
        SortingApp sorter = new SortingApp(arr);
        sorter.sort();
        String expected = "0 1 2 3 4 5 6 7 8 9 ";
        assertEquals(expected, sorter.getResult());
    }

    @Test
    public void testMoreThanTenElementsArray() {
        String[] arr =  {"9", "7", "2", "8", "1", "6", "4", "5", "3", "0", "44"};
        SortingApp sorter = new SortingApp(arr);
        sorter.sort();
        String expected = "Error: Too many arguments. Maximum of 10 arguments allowed.";
        assertEquals(expected, sorter.getResult());
    }
}
