package com.epam;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortingAppParameterizedTest {
    private int[] inputArray;

    public SortingAppParameterizedTest(int[] inputArray) {
        this.inputArray = inputArray;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new int[]{4, 2, 1, 5, 3}},
                {new int[]{9, 7, 2, 8, 1}},
                {new int[]{5, 1, 3, 6, 4}}
        });
    }

    @Test
    public void testBubbleSort() {
        BubbleSort.bubbleSort(inputArray);
        int[] expectedArray = Arrays.copyOf(inputArray, inputArray.length);
        Arrays.sort(expectedArray);
        assertArrayEquals(expectedArray, inputArray);
    }
}
