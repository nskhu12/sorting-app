# Sorting App

Complete the task to get a practical grasp of Apache Maven and its major features. You will need to create a Maven-based project – Sorting App. It is a small Java application that takes up to ten command-line arguments as integer values, sorts them in the ascending order, and then prints them into standard output.

It will take you about 3 hours to complete the task.

Please be aware that this task status is mandatory.

Please read carefully and do the following:

Create a Maven project and specify its GAV settings, encoding, language level, etc.
Write the code implementing the app specification.
Configure the Maven project to build a runnable jar containing application and its dependencies.
Share the project using a public GitLab repository.
Submit a link to your repository.